#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <ctype.h>
#include <mqueue.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <semaphore.h>

#define MAX_SIZE 1024
#define MAX_LINE_LENGTH 100


sem_t* semaphore;  // Semaphore untuk membatasi akses ke playlist
sem_t* system_overload;  // Semaphore untuk melacak jumlah pengguna aktif

void base64Decode(const unsigned char* input, int length, unsigned char* output) {
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new_mem_buf(input, length);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decoded_length = BIO_read(bio, output, length);
    output[decoded_length] = '\0'; // Menambahkan NULL character di akhir string
    BIO_free_all(bio);
}

void hexDecode(const unsigned char* input, int length, unsigned char* output) {
    int i;
    for (i = 0; i < length; i += 2) {
        sscanf((char*)&input[i], "%2hhx", &output[(i - 1) / 2]); // Menggunakan (i - 1) / 2 untuk indeks output
    }
}

void decryptPlaylist() {
    FILE* input_file = fopen("song-playlist.json", "r");
    FILE* output_file = fopen("playlist.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Error opening files");
        exit(1);
    }

    char line[MAX_SIZE];
    while (fgets(line, MAX_SIZE, input_file) != NULL) {
        // Proses decrypt dengan metode ROT13
        if (strstr(line, "\"method\": \"rot13\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 4);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            for (int i = 0; i < strlen(song); i++) {
                if (isalpha(song[i])) {
                    if ((song[i] >= 'a' && song[i] <= 'm') || (song[i] >= 'A' && song[i] <= 'M')) {
                        song[i] += 13;
                    } else {
                        song[i] -= 13;
                    }
                }
            }
            fprintf(output_file, "%s\n", song);
            free(song);
        }
        // Proses decrypt dengan metode Base64
        else if (strstr(line, "\"method\": \"base64\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            unsigned char base64_decoded[MAX_SIZE];
            base64Decode((const unsigned char*)song, strlen((const char*)song), base64_decoded);
            fprintf(output_file, "%s\n", base64_decoded);
            free(song);
        }
        // Proses decrypt dengan metode Hexadecimal
        else if (strstr(line, "\"method\": \"hex\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            unsigned char hex_decoded[MAX_SIZE];
            hexDecode((const unsigned char*)song, strlen((const char*)song), hex_decoded);
            fprintf(output_file, "%s\n", hex_decoded);
            free(song);
        }
    }

    fclose(input_file);
    fclose(output_file);

    system("sort -o playlist.txt playlist.txt");

    printf("Playlist decrypted successfully!\n");
}

void listSongs() {
    FILE* playlist_file = fopen("playlist.txt", "r");

    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    char line[MAX_SIZE];
    int song_number = 1;

    while (fgets(line, MAX_SIZE, playlist_file) != NULL) {
        // Hapus karakter newline pada akhir string
        if (line[strlen(line) - 1] == '\n') {
            line[strlen(line) - 1] = '\0';
        }

        // Tambahkan lagu ke dalam playlist
        printf("%d. %s\n", song_number, line);
        song_number++;
    }

    fclose(playlist_file);
}

void playSong(char* song, pid_t user_id) {
    FILE* playlist_file = fopen("playlist.txt", "r");

    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    char line[MAX_LINE_LENGTH];
    int count = 0;
    char found_songs[MAX_SIZE][MAX_LINE_LENGTH];

    while (fgets(line, sizeof(line), playlist_file) != NULL) {
        // Menghapus karakter whitespace di akhir baris
        int len = strlen(line);
        while (len > 0 && isspace(line[len - 1])) {
            line[--len] = '\0';
        }

        // Memeriksa apakah baris mengandung query
        if (strcasestr(line, song) != NULL) {
            strncpy(found_songs[count], line, MAX_LINE_LENGTH - 1);
            found_songs[count][MAX_LINE_LENGTH - 1] = '\0';
            count++;
            if (count >= MAX_SIZE) {
                break; // Jika sudah mencapai batas maksimum lagu, keluar dari loop
            }
        }
    }

    fclose(playlist_file);

    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song);
    } else if (count == 1) {
        printf("USER <%ld> PLAYING \"%s\"\n", (long) user_id, found_songs[0]);
    } else {
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\":\n", count, song);
        for (int i = 0; i < count; i++) {
            printf("%d. %s\n", i+1, found_songs[i]);
        }
    }
}

void addSong(char* song, pid_t user_id) {
    FILE* playlist_file = fopen("playlist.txt", "r+");
    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    // Cek apakah lagu sudah ada dalam playlist
    char line[MAX_SIZE];
    while (fgets(line, MAX_SIZE, playlist_file) != NULL) {
        // Hapus karakter newline pada akhir string
        if (line[strlen(line) - 1] == '\n') {
            line[strlen(line) - 1] = '\0';
        }

        // Periksa apakah lagu sudah ada dalam playlist
        if (strcasecmp(line, song) == 0) {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist_file);
            return;
        }
    }

    // Tambahkan lagu ke dalam playlist
    fseek(playlist_file, 0, SEEK_END);
    fprintf(playlist_file, "%s\n", song);
    printf("USER %d ADD <%s>\n", user_id, song);

    fclose(playlist_file);
}

int main() {
    pid_t user_id = getpid();

    // Membuat dan menginisialisasi semaphore
    semaphore = sem_open("/stream_semaphore", O_CREAT, 0644, 2);
    if (semaphore == SEM_FAILED) {
        perror("Error creating/opening the semaphore");
        exit(1);
    }

    system_overload = sem_open("/system_overload_semaphore", O_CREAT, 0644, 0);
    if (system_overload == SEM_FAILED) {
        perror("Error creating/opening the semaphore");
        exit(1);
    }

    mqd_t mq;
    char buffer[MAX_SIZE];
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_SIZE;
    attr.mq_curmsgs = 0;

    mq = mq_open("/stream_queue", O_CREAT | O_RDONLY, 0644, &attr);
    if (mq == -1) {
        perror("Error opening the message queue");
        exit(1);
    }

    while (1) {
        ssize_t bytes_read = mq_receive(mq, buffer, MAX_SIZE, NULL);
        if (bytes_read == -1) {
            perror("Error receiving message from the queue");
            exit(1);
        }

        buffer[bytes_read] = '\0';  // Null-terminate the received message

        // Acquire semaphore sebelum memproses perintah
        sem_wait(semaphore);

        // Cek jumlah pengguna aktif
        int active_users;
        sem_getvalue(system_overload, &active_users);

        // Pengecekan perintah dari user
        if (strcmp(buffer, "DECRYPT") == 0) {
            decryptPlaylist();
        } else if (strcmp(buffer, "LIST") == 0) {
            listSongs();
        } else if (strncmp(buffer, "PLAY \"", 6) == 0) {
            if (active_users < 2) {
                char* start = buffer + 6;
                char* end = strchr(start, '\"');
                if (end != NULL) {
                    *end = '\0';
                    char* song = start;
                    playSong(song, user_id);
                } else {
                    printf("UNKNOWN COMMAND\n");
                }
            } else {
                printf("STREAM SYSTEM OVERLOAD\n");
            }
        } else if (strncmp(buffer, "ADD <", 5) == 0) {
            if (active_users < 2) {
                char* start = buffer + 5;
                char* end = strchr(start, '>');
                if (end != NULL) {
                    *end = '\0';
                    char* song = start;
                    addSong(song, user_id);
                } else {
                    printf("UNKNOWN COMMAND\n");
                }
            } else {
                printf("STREAM SYSTEM OVERLOAD\n");
            }
        }
        else {
            printf("UNKNOWN COMMAND\n");
        }

        // Release semaphore setelah selesai memproses perintah
        sem_post(semaphore);
    }

    // Menghapus semaphore
    sem_close(semaphore);
    sem_close(system_overload);
    sem_unlink("/stream_semaphore");
    sem_unlink("/system_overload_semaphore");

    mq_close(mq);
    mq_unlink("/stream_queue");

    return 0;
}


