#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#define MAX_SIZE 1024

int main() {
    mqd_t mq;
    char buffer[MAX_SIZE];

    mq = mq_open("/stream_queue", O_WRONLY);
    if (mq == -1) {
        perror("Error opening the message queue");
        exit(1);
    }

    while (1) {
        printf("Enter command: ");
        fgets(buffer, MAX_SIZE, stdin);
        buffer[strcspn(buffer, "\n")] = '\0'; 

        mq_send(mq, buffer, MAX_SIZE, 0);
    }

    mq_close(mq);

    return 0;
}
