#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#define MAX_CHAR 256

/*  STRUCT HUFFMAN TREE dengan cara:
 gabungkan 2 node yg memiliki frekuensi paling rendah pada setiap tahapnya */

struct Node {
    // fungsi data digunakan untuk menyimpan karakter yg akan dikompresi
    char data;
    // fungsi frekuensi digunakan untuk menyimpan jumlah kemunculan karakter saat dikompresi
    unsigned frekuensi;
    struct Node *left;
    struct Node *right;
};

/* Hitung frekuensi tiap karakter dalam sebuah file
 definisikan fungsi dengan 2 parameter yaitu nama file dan array frekuensi yang menyimpan frekuensi karakter*/

void count_frekuensi(const char* namafile, unsigned int frekuensi[MAX_CHAR]) {

    // buka file dengan mode r (read only) dan simpan file ke pointer bernama FILE
    FILE *file = fopen(namafile, "r");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (file == NULL) {
        exit(1);
    }

    //variabel c dengan tipe data integer
    int c;
    // baca karakter satu persatu dari file yang dibuka sebelumnya hingga akhir file (EOF) tercapai
    while ((c = fgetc(file)) != EOF) {
        // menambah jumlah frekuensi karakter pada array frekuensi sesuai dengan karakter yang dibaca
        frekuensi[c]++;
    }
    //menutup file yang telah dibuka sebelumnya dengan funsi fclose
    fclose(file);
}

// buat node baru dalam tree
struct Node* create_node(char data, unsigned frekuensi) {
    //membuat pointer node dan mengalokasikan memori untuk struct node
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    //menginisialisasi node kiri dan kanan sebagai NULL
    node->left = node->right = NULL;
    //node data adalah parameter data
    node->data = data;
    //inisialisasi node frekuensi adalah freq
    node->frekuensi = frekuensi;
    //mengembalikan pointer ke node baru
    return node;
}

// gabungkan dua node yang memiliki frequency paling kecil
struct Node* mergeNode(struct Node* left, struct Node* right) {
    //membuat node baru dengan karakter kosong dan freq dari dua node yang digabungkan
    struct Node* node = create_node('\0', left->frekuensi + right->frekuensi);
    //menghubungkan node kiri sebagai child kiri mode baru
    node->left = left;
    //menghubungkan node kanan sebagai child kanan mode baru
    node->right = right;
    //mengembalikan pointer ke node baru
    return node;
}

// membuat huffman tree
struct Node* create_huffman(unsigned int frekuensi[MAX_CHAR]) {
    //membuat array node dengan ukuran MAX_CHAR
    struct Node *nodes[MAX_CHAR];
    int x, y;
    //looping sebanyak ukuran MAX_CHAR
    for (x = 0, y = 0; x < MAX_CHAR; x++) {
	//jika drequency lebih dari 0 maka,
        if (frekuensi[x] > 0) {
	    //membuat node baru dengan karakter i dan freq dari i
            nodes[y] = create_node(x, frekuensi[x]);
	    //increment variabel j
            y++;
        }
    }

    //inisialisasi size dengan j
    int size = y;
    //loop selama size lebih dari 1
    while (size > 1) {
	//mengambil node kiri dari indeeks size - 2
        struct Node *left = nodes[size - 2];
	//mengambil node kanan dari indeks size - 1
        struct Node *right = nodes[size - 1];
	//menggabungkan 2 node dan membuat node baru
        struct Node *merged = mergeNode(left, right);
	//mengganti node kiri dengan node baru
        nodes[size - 2] = merged;
	//decrement size
        size--;
    }

    //mengembalikan pointer ke root node
    return nodes[0];
}

// fungsi untuk membuat huffman code
// definisikan fungsi generate_code dengan parameter root sebagai pointer ke node root dari hufman tree,
// code sebagai array untuk menyimpan kode huffman, depth bertipe integer untuk menyimpan kedalaman dari node dalam hufman tree, 
// huffmancode sebagai array of pointer ke character untuk menyimpan kode huffman
void generate_code(struct Node* root, char* code, int depth, char* huffmanCode[MAX_CHAR]) {
    //jika node saat ini kosong, maka kembalikan fungsi
    if (root == NULL) {
	//kembalikan fungsi
        return;
    }

    //jika node saat ini adalah tidak memiliki child kiri dan kanan maka lanjut
    if (root->left == NULL && root->right == NULL) {
	//set karakter null pada indeks depth dari code untuk menandakan akhir dari kode huffman
        code[depth] = '\0';
	//salin kode huffman dari code ke huffmancode pada indeks yang sesuai dengan karakter saat ini
        huffmanCode[root->data] = strdup(code);
    }

    //set karakter 0 pada indeks depth dari code untuk menandakan pergerakan ke child kiri dalam huffman tree
    code[depth] = '0';
    //rekursif untuk menghasilkan kode huffman untuk child kiri dari node saat ini
    generate_code(root->left, code, depth + 1, huffmanCode);

    //set karakter 1 pada indeks depth dari code untuk menandakan pergerakan ke child kanan dalam huffman tree
    code[depth] = '1';
    //jika depth lebih besar dari 0 maka huffmancode pada indeks karakter saat ini telah dialokasikan dan harus dihapus
    generate_code(root->right, code, depth + 1, huffmanCode);

    if (depth > 0) {
        free(huffmanCode[root->data]);
    }
}

// simpan huffman tree ke file yang sudah dicompress
// buat fungsi save_huffman dengan parameter root bertipe pointer ke node dan file bertipe pointer ke FILE
void save_huffman(struct Node* root, FILE* file) {
    //jika root adalah NULL maka keluar dari fungsi
    if (root == NULL) {
        return;
    }

    // apabila root tidak memiliki child kiri dan kanan maka,
    if (root->left == NULL && root->right == NULL) {
	//tulis karakter 1 kedalam file
        fputc('1', file);
	//tulis karakter yang disimpan pada node ke dalam file
        fputc(root->data, file);
    // apabila root memiliki child kiri dan kanan maka,
    } else {
	//tulis karakter 0 ke dalam file dan panggil fungsi save_huffman untuk setiap child kiri dan kanan dengan melewatkan file sebagai parameter
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}

// fungsi kompres file
//fungsi compress_file() memiliki 3 parameter yaitu input_namafile untuk nama file input, output_namafile untuk nama file output, dan huffmancode array yang berisi kode  huffman untuk setiap karakter
void compress_file(const char* input_namafile, const char* output_namafile, char* huffmanCode[MAX_CHAR]) {
    //membuka file input dalam mode read (r)
    FILE* input_file = fopen(input_namafile, "r");
    //jika file input tidak dapat dibuka
    if (input_file == NULL) {
        exit(1);
    }
    //membuka file output dalam mode wb (menulis dalam mode binary)
    FILE* output_file = fopen(output_namafile, "wb");
    //jika file output tidak dapat dibuat 
    if (output_file == NULL) {
        exit(1);
    }
    int c;
    //looping membaca setiap karakter dalam file input sampai mencapai akhir file (EOF)
    while ((c = fgetc(input_file)) != EOF) {
	//menyimpan kode huffman untuk karakter saat ini dalam variabel code
        char* code = huffmanCode[c];
	//menuliskan kode Huffman ke file output menggunakan fungsi fwrite()
        fwrite(code, sizeof(char), strlen(code), output_file);

    }
    //menutup file input dan output setelah selesai digunakan menggunakan fungsi fclose()
    fclose(input_file);
    fclose(output_file);
}

//fungsi utama untuk melakukan kompresi file
int main() {
    // SOAL A 
    /* A. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. 
    Kirim hasil perhitungan frekuensi tiap huruf ke child process.
    */
    // pertama tama definisikan nama file input dan output yang akan digunakan dalam proses kompresi
    const char* input_namafile = "file.txt";
    const char* compressed_namafile = "compressed_file.txt";

    // definisi array freq yang menyimpan frekuensi setiap karakter dalam file input
    unsigned int frekuensi[MAX_CHAR] = {0};
    //memanggil fungsi count_freq untuk menghitung frekuensi karakter dalam file input dan simpan dalam array freq
    count_frekuensi(input_namafile, frekuensi);

    // panggil fungsi create_huffman untuk buat huffman tree berdasarkan frekuensi karakter dalam file input
    struct Node* root = create_huffman(frekuensi);

    // buat pipe untuk komunikasi antara parent dan child proses
    int pipe_fd[2];
    //mengecek apakah pembuatan pipe berhasil
    if (pipe(pipe_fd) == -1) {
        exit(1);
    }

    // buat child proses
    pid_t pid = fork();

    //mengecek apakah pembuatan child proses berhasil
    if (pid < 0) {
        exit(1);
    //bagian parent proses
    } else if (pid > 0) {
	//menutup file descriptor yang tidak digunakan pada pipe
        close(pipe_fd[1]);
        // definisikan array child_freq untuk menyyimpan frekuensi karakter dari child proses
        unsigned int child_frekuensi[MAX_CHAR];
	//membaca frekuensi karakter dari child proses dan menyimpannya dalam array
        read(pipe_fd[0], child_frekuensi, sizeof(child_frekuensi));
        // definisikan variabel original_bits untuk menyimpan jumlah bit pada file input sebelum dikompresi
        unsigned long long int original_bits = 0;

	//menghitung jumlah bit pada file input sebelum dikompresi dan menyimpan dalam variabel original_bits
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += frekuensi[i] * sizeof(char) * 8;
        }

	//definisikan array huffmancode untuk menyyimpan kode huffman pada setiap karakter
        char* huffmanCode[MAX_CHAR] = {NULL};
	//definisikan array code untuk menyimpan kode huffman sementara
        char code[MAX_CHAR];
	//memanggil fungsi generate_code untuk menghasilkan kode huffman setiap karakter dan menyimpannya dalam array huffmancode
        generate_code(root, code, 0, huffmanCode);

	// membuka file output untuk menyimpan file yang sudah dikompresi
        FILE* compressed_file = fopen(compressed_namafile, "wb");
        if (compressed_file == NULL) {
            exit(1);
        }
	//memanggil fungsi save_huffman untuk menyimpan huffman tree ke dalam file output
        save_huffman(root, compressed_file);
	//menutup file output
        fclose(compressed_file);
	//memanggil fungsi compress_file untuk melakukan kompresi file input dan menyimpannya ke dalam file output
        compress_file(input_namafile, compressed_namafile, huffmanCode);

        // simpan jumlah bit setelah file di compress menggunakan algoritma huffman
        unsigned long long int compressed_bits = 0;
	//looping untuk memproses setiap karakter pada file dengan menghitung jumlah bit setiap karakter
        for (int i = 0; i < MAX_CHAR; i++) {
	    //jika frekuensi karakter >0 dan kodeHuffman tidak NULL
            if (frekuensi[i] > 0 && huffmanCode[i] != NULL) {
		//program akan menghitung jumlah bit untuk karakter tersebut dengan mengalikan frekuensi karakter dengan panjang kode Huffman
                compressed_bits += frekuensi[i] * strlen(huffmanCode[i]);
            }
        }
    /*D. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman 
    dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
    */
	//tampilkan jumlah bit pada file asli dan file hasil kompresi dengan format specifier %llu (long long int)
        printf("total original file: %llu\n", original_bits);
        printf("total compressed file: %llu\n", compressed_bits);

        // tunggu child process
        wait(NULL);

        //child process
    } else {
        close(pipe_fd[0]);

        // tulis frekuensiuency karakter ke parent process
        write(pipe_fd[1], frekuensi, sizeof(frekuensi));
        exit(0);
    }

    return 0;
}
