#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>>

struct shareMem{
    int shareMatrix[4][5];
};

int matrix_hasil[4][5];

void main(){
    key_t key = 1244;
    int shmid = shmget(key,sizeof(struct shareMem),0666);
    struct shareMem *shmptr;
    shmptr = (struct shareMem *) shmat(shmid, NULL, 0);
    
    memcpy(matrix_hasil, &shmptr->shareMatrix, 20 * sizeof(int));
    
    printf("Hasil perkalian matrix dari program cinta\n");
    int i, j, k;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 5; j++){
            printf("%d ", matrix_hasil[i][j]);
        }
        printf("\n");
    }
}
