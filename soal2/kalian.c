#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int matrix_1[4][2];
int matrix_2[2][5];
int mHasil_kali[4][5];

void isiMatrix_1(int baris, int kolom){
    srand(time(NULL));
    
    for (int i = 0; i < baris; i++){
        for (int j = 0; j < kolom; j++){
            matrix_1[i][j] = (rand() %  5) + 1;
        }
    }
}

void isiMatrix_2(int baris, int kolom){
    srand(time(NULL));
    
    for (int i = 0; i < baris; i++){
        for (int j = 0; j < kolom; j++){
            matrix_2[i][j] = (rand() %  4) + 1;
        }
    }
}

void perkalian(){
    int i, j, k;
    
    for (i = 0; i < 4; i++){
        for (j = 0; j < 5; j++){
            mHasil_kali[i][j] = 0;
            for (k = 0; k < 2; k++){
                mHasil_kali[i][j] += matrix_1[i][k] * matrix_2[k][j];
            }
        }
    }
}

struct shareMem{
    int shareMatrix[4][5];
};

void main() {
	// your code goes here
	
	isiMatrix_1(4, 2);
	isiMatrix_2(2, 5);
	perkalian();
	
	printf("hasil perkalian dari program kalian\n");
	for(int i = 0; i < 4; i++){
	    for(int j = 0; j < 5; j++){
	        printf("%d ", mHasil_kali[i][j]);
	    }
	    printf("\n");
	}
	printf("\n");
	
	key_t key = 1244;
	int shmid = shmget(key, sizeof(struct shareMem), IPC_CREAT|0666);
	struct shareMem *shmptr;
	shmptr = (struct shareMem *) shmat(shmid, NULL, 0);
	
	for(int i = 0; i < 4; i++){
	    for(int j = 0; j < 5; j++){
	        shmptr->shareMatrix[i][j] = mHasil_kali[i][j];
	    }
	}
	
	sleep(8);
	shmdt((void *) shmptr);
	shmctl(shmid, IPC_RMID, NULL);
}

