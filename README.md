# sisop-praktikum-modul-3-2023-ws-f09



##  Soal 1

Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 


#### Poin 1A

Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) 
{
    FILE *file = fopen(filename, "r");
    
    if (file == NULL) {
        exit(1);
    }
    int c;
    while ((c = fgetc(file)) != EOF) {
        // menambah jumlah frekuensi karakter pada array freq sesuai dengan karakter yang dibaca
        freq[c]++;
    }
    fclose(file);
}

```

#### Penjelasan

Fungsi `count_freq()` membuka sebuah file dengan mode "r" (hanya baca) dan membaca satu karakter demi satu dari file tersebut sampai mencapai akhir file (EOF). Setiap karakter yang dibaca akan meningkatkan jumlah frekuensi kemunculan karakter dalam array freq. Array freq digunakan untuk menyimpan frekuensi kemunculan setiap karakter dalam file yang akan dikompresi. Array ini akan dikirim ke child process untuk dihitung lebih lanjut.

#### Poin 1B
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

````
[MAX_CHAR]) {
        FILE *inputFile = fopen(filename, "r");    if (inputFile == NULL) {
        exit(1);
    }
    FILE *outputFile = fopen("compressed.txt", "w");
    if (outputFile == NULL) {
        exit(1);
    }
    int c;
    while ((c = fgetc(inputFile)) != EOF) {

        fputs(huffCode[c], outputFile);
    }
    fclose(inputFile);
    fclose(outputFile);
}
````

#### Penjelasan
Fungsi yang tertera di atas berguna untuk menulis data ke dalam sebuah file compressed dengan menggunakan algoritma Huffman. Fungsi ini akan membuka file dengan mode "read only" dan membaca satu karakter demi satu dari file tersebut hingga mencapai akhir file. Setelah itu, fungsi akan menuliskan kode Huffman untuk karakter yang dibaca ke dalam file compressed. Selain itu, fungsi ini akan menutup file yang telah dibuka sebelumnya dengan menggunakan fungsi fclose.

#### Poin 1C

Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

````
if (pid == 0) {
    int fd = open("compressed", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    write(fd, freq, MAX_CHAR * sizeof(unsigned int));
    struct Node *root = create_huffman(freq);
    for (int i = 0; i < MAX_CHAR; i++) {
        huffCode[i] = NULL;
    }
    char code[MAX_CHAR];
    code[0] = '\0';
    generate_code(root, code, 0, huffCode);
    int nread;
    while ((nread = read(fd_input, buffer, BLOCK)) > 0) {
        for (int i = 0; i < nread; i++) {            char* code = huffCode[buffer[i]];
            int len = strlen(code);
            for (int j = 0; j < len; j++) {
                write(fd, &code[j], 1);
            }
        }
    }
    close(fd);
    exit(0);
}
````
#### Penjelasan
Dalam potongan kode di atas, terlihat bagaimana di child process dibuat dan disimpan Huffman tree pada file yang sudah terkompresi. Selanjutnya, setiap karakter pada file yang akan dikompresi akan diubah menjadi kode Huffman dan dikirim ke program dekompresi menggunakan pipe.

#### Point 1D
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
````
int pipe_fd[2];


if (pipe(pipefd) == -1) {
    perror("pipe");
    exit(1);
}


pid_t pid = fork();

if (pid == 0) {
    // tutup bagian tulis dari pipe
    close(pipefd[1]);

    // redirect input dari pipe
    dup2(pipefd[0], STDIN_FILENO);

    // jalankan program dekompresi
    char *args[] = {"./decompress", NULL};
    execvp(args[0], args);

    // keluar jika gagal
    perror("execvp");
    exit(1);
} else {
    close(pipefd[0]);

    write(pipefd[1], compressed_data, compressed_size);

    close(pipefd[1]);

    wait(NULL);
    FILE *in = fopen(argv[1], "rb");
    struct Node *root = read_tree(in);

    char *huffman_code[MAX_CHAR];

    read(pipefd[0], huffman_code, sizeof(huffman_code));
    decompress(huffman_code, compressed_size, root);
}
````
#### Penjelasan
Potongan kode di atas melakukan pembuatan pipe untuk mengirimkan hasil kompresi dari child process ke parent process. Hasil kompresi kemudian ditulis ke sisi penulisan (tulis) dari pipe. Di parent process, Huffman tree dibaca dari file terkompresi menggunakan fungsi read_tree() dan hasilnya disimpan di dalam variabel root. Selanjutnya, kode Huffman dibaca dari sisi pembacaan (baca) dari pipe dan disimpan di dalam array huffman_code. Akhirnya, dilakukan proses dekompresi menggunakan fungsi decompress().

#### Point 1E
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

````
if (pid > 0) {
    wait(NULL);
    struct stat st;
    stat(argv[1], &st);
    double size_before = st.st_size * 8;
    double size_after = count_bits(huffCode, freq) * 1.0;
    printf("Ukuran file sebelum kompresi: %.2lf bit\n", size_before);
    printf("Ukuran file setelah kompresi: %.2lf bit\n", size_after);
    printf("Rasio kompresi: %.2lf%%\n", (size_before - size_after) / size_before * 100);
}
`````
##### Penjelasan
Program menunggu proses child selesai dengan menggunakan wait(NULL) untuk memastikan bahwa proses child telah selesai sebelum dilanjutkan. Setelah itu, program menghitung ukuran file sebelum dan setelah dikompresi dengan menggunakan fungsi count_bits(). Fungsi ini menghitung jumlah bit setelah kompresi dengan menggunakan algoritma Huffman. Kemudian, program menampilkan perbandingan ukuran file sebelum dan setelah dikompresi, serta rasio kompresi dalam persen.

## Soal 2

## Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

#### 3A
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

**user.c**
``` c
int main() {
    mqd_t mq;
    char buffer[MAX_SIZE];

    mq = mq_open("/stream_queue", O_WRONLY);
    if (mq == -1) {
        perror("Error opening the message queue");
        exit(1);
    }

    //

    mq_close(mq);

    return 0;
}

```

**stream.c**
``` c
int main() {

    mqd_t mq;
    char buffer[MAX_SIZE];
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_SIZE;
    attr.mq_curmsgs = 0;

    mq = mq_open("/stream_queue", O_CREAT | O_RDONLY, 0644, &attr);
    if (mq == -1) {
        perror("Error opening the message queue");
        exit(1);
    }

    //

    mq_close(mq);
    mq_unlink("/stream_queue");

    return 0;
}
```

#### 3B
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
``` c
void base64Decode(const unsigned char* input, int length, unsigned char* output) {
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new_mem_buf(input, length);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decoded_length = BIO_read(bio, output, length);
    output[decoded_length] = '\0'; // Menambahkan NULL character di akhir string
    BIO_free_all(bio);
}

void hexDecode(const unsigned char* input, int length, unsigned char* output) {
    int i;
    for (i = 0; i < length; i += 2) {
        sscanf((char*)&input[i], "%2hhx", &output[(i - 1) / 2]); // Menggunakan (i - 1) / 2 untuk indeks output
    }
}

void decryptPlaylist() {
    FILE* input_file = fopen("song-playlist.json", "r");
    FILE* output_file = fopen("playlist.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Error opening files");
        exit(1);
    }

    char line[MAX_SIZE];
    while (fgets(line, MAX_SIZE, input_file) != NULL) {
        // Proses decrypt dengan metode ROT13
        if (strstr(line, "\"method\": \"rot13\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 4);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            for (int i = 0; i < strlen(song); i++) {
                if (isalpha(song[i])) {
                    if ((song[i] >= 'a' && song[i] <= 'm') || (song[i] >= 'A' && song[i] <= 'M')) {
                        song[i] += 13;
                    } else {
                        song[i] -= 13;
                    }
                }
            }
            fprintf(output_file, "%s\n", song);
            free(song);
        }
        // Proses decrypt dengan metode Base64
        else if (strstr(line, "\"method\": \"base64\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            unsigned char base64_decoded[MAX_SIZE];
            base64Decode((const unsigned char*)song, strlen((const char*)song), base64_decoded);
            fprintf(output_file, "%s\n", base64_decoded);
            free(song);
        }
        // Proses decrypt dengan metode Hexadecimal
        else if (strstr(line, "\"method\": \"hex\"")) {
            fgets(line, MAX_SIZE, input_file);
            char* song = malloc(strlen(line) - 5);
            strncpy(song, line + 12, strlen(line) - 15);
            song[strlen(line) - 15] = '\0'; // Memastikan string diakhiri dengan null character
            unsigned char hex_decoded[MAX_SIZE];
            hexDecode((const unsigned char*)song, strlen((const char*)song), hex_decoded);
            fprintf(output_file, "%s\n", hex_decoded);
            free(song);
        }
    }

    fclose(input_file);
    fclose(output_file);

    system("sort -o playlist.txt playlist.txt");

    printf("Playlist decrypted successfully!\n");
}
```

#### 3C
User dapat mengirimkan perintah LIST
``` c
void listSongs() {
    FILE* playlist_file = fopen("playlist.txt", "r");

    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    char line[MAX_SIZE];
    int song_number = 1;

    while (fgets(line, MAX_SIZE, playlist_file) != NULL) {
        // Hapus karakter newline pada akhir string
        if (line[strlen(line) - 1] == '\n') {
            line[strlen(line) - 1] = '\0';
        }

        // Tambahkan lagu ke dalam playlist
        printf("%d. %s\n", song_number, line);
        song_number++;
    }

    fclose(playlist_file);
}
```

### 3D
User juga dapat mengirimkan perintah PLAY <SONG>
``` c
void playSong(char* song, pid_t user_id) {
    FILE* playlist_file = fopen("playlist.txt", "r");

    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    char line[MAX_LINE_LENGTH];
    int count = 0;
    char found_songs[MAX_SIZE][MAX_LINE_LENGTH];

    while (fgets(line, sizeof(line), playlist_file) != NULL) {
        // Menghapus karakter whitespace di akhir baris
        int len = strlen(line);
        while (len > 0 && isspace(line[len - 1])) {
            line[--len] = '\0';
        }

        // Memeriksa apakah baris mengandung query
        if (strcasestr(line, song) != NULL) {
            strncpy(found_songs[count], line, MAX_LINE_LENGTH - 1);
            found_songs[count][MAX_LINE_LENGTH - 1] = '\0';
            count++;
            if (count >= MAX_SIZE) {
                break; // Jika sudah mencapai batas maksimum lagu, keluar dari loop
            }
        }
    }

    fclose(playlist_file);

    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song);
    } else if (count == 1) {
        printf("USER <%ld> PLAYING \"%s\"\n", (long) user_id, found_songs[0]);
    } else {
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\":\n", count, song);
        for (int i = 0; i < count; i++) {
            printf("%d. %s\n", i+1, found_songs[i]);
        }
    }
}
```

#### 3E
User juga dapat menambahkan lagu ke dalam playlist
``` c
void addSong(char* song, pid_t user_id) {
    FILE* playlist_file = fopen("playlist.txt", "r+");
    if (playlist_file == NULL) {
        perror("Error opening playlist file");
        exit(1);
    }

    // Cek apakah lagu sudah ada dalam playlist
    char line[MAX_SIZE];
    while (fgets(line, MAX_SIZE, playlist_file) != NULL) {
        // Hapus karakter newline pada akhir string
        if (line[strlen(line) - 1] == '\n') {
            line[strlen(line) - 1] = '\0';
        }

        // Periksa apakah lagu sudah ada dalam playlist
        if (strcasecmp(line, song) == 0) {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist_file);
            return;
        }
    }

    // Tambahkan lagu ke dalam playlist
    fseek(playlist_file, 0, SEEK_END);
    fprintf(playlist_file, "%s\n", song);
    printf("USER %d ADD <%s>\n", user_id, song);

    fclose(playlist_file);
}
```

#### 3F & 3G
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
``` c
int main() {
    pid_t user_id = getpid();

    // Membuat dan menginisialisasi semaphore
    semaphore = sem_open("/stream_semaphore", O_CREAT, 0644, 2);
    if (semaphore == SEM_FAILED) {
        perror("Error creating/opening the semaphore");
        exit(1);
    }

    system_overload = sem_open("/system_overload_semaphore", O_CREAT, 0644, 0);
    if (system_overload == SEM_FAILED) {
        perror("Error creating/opening the semaphore");
        exit(1);
    }
    //
    //
    //

            // Acquire semaphore sebelum memproses perintah
        sem_wait(semaphore);

        // Cek jumlah pengguna aktif
        int active_users;
        sem_getvalue(system_overload, &active_users);

        // Pengecekan perintah dari user
        if (strcmp(buffer, "DECRYPT") == 0) {
            decryptPlaylist();
        } else if (strcmp(buffer, "LIST") == 0) {
            listSongs();
        } else if (strncmp(buffer, "PLAY \"", 6) == 0) {
            if (active_users < 2) {
                char* start = buffer + 6;
                char* end = strchr(start, '\"');
                if (end != NULL) {
                    *end = '\0';
                    char* song = start;
                    playSong(song, user_id);
                } else {
                    printf("UNKNOWN COMMAND\n");
                }
            } else {
                printf("STREAM SYSTEM OVERLOAD\n");
            }
        } else if (strncmp(buffer, "ADD <", 5) == 0) {
            if (active_users < 2) {
                char* start = buffer + 5;
                char* end = strchr(start, '>');
                if (end != NULL) {
                    *end = '\0';
                    char* song = start;
                    addSong(song, user_id);
                } else {
                    printf("UNKNOWN COMMAND\n");
                }
            } else {
                printf("STREAM SYSTEM OVERLOAD\n");
            }
        }
        else {
            printf("UNKNOWN COMMAND\n");
        }

        // Release semaphore setelah selesai memproses perintah
        sem_post(semaphore);
    }

    // Menghapus semaphore
    sem_close(semaphore);
    sem_close(system_overload);
    sem_unlink("/stream_semaphore");
    sem_unlink("/system_overload_semaphore");

    //
    //
    return 0;
}
```
